const path = require('path');

module.exports = {
    loading: {
        color: '#92D3CE',
    },
    
    rootDir: path.resolve(__dirname),

    srcDir: path.join(__dirname, 'client'),

    dev: process.env.NODE_ENV !== 'production',
    
    router: {
        linkActiveClass: 'is-active',
        middleware: 'check-auth'
    },

    head: {
        title: 'Kiều Sơn Lâm',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Meta description' }
        ],
        link: [
            { rel: 'stylesheet', href: '//fonts.googleapis.com/icon?family=Material+Icons' }
        ]
    },

    build: {
        vendor: ['buefy']
    },

    plugins: ['~plugins/buefy'],

    css: [
        'buefy/lib/buefy.css'
    ]
};