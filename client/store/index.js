import Vuex from 'vuex'

import auth from './modules/auth'
import articles from './modules/articles'

import admin_articles from './modules/admin_articles'

const store = new Vuex.Store({
  modules: {
    auth,
    articles,
    admin_articles
  }
})

export default store