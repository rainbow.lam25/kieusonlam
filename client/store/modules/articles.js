import client from '../feathers';

export default {
  state: {
    total: 0,
    limit: 5,
    skip: 0,
    data: [],
    currentPage: 1,
    single: null
  },

  actions: {
    async nuxtServerInit ({ dispatch }, { req }) {
        await dispatch('FETCH_ARTICLES', { page: 1 });
    },
    FETCH_ARTICLES: ({ commit, state }, { page = 1, sort }) => {
        commit('SET_CURRENT_PAGE', page)
        const skipPage = state.limit * (page - 1);
        let sortQuery =  { createdAt: -1 };
        if (sort) {
            sortQuery = { 
                [sort.field]: sort.order 
            }
        }
        return client.service('api/articles').find({
            query: {
                $limit: state.limit,
                $sort: sortQuery,
                $skip: skipPage
            }
        }).then(response => {
            commit('SET_ARTICLES', { response })
        })
    },

    ADD_NEW_ARTICLE: ({ commit, dispatch, state }, { title, content, status }) => {
        return new Promise((resolve, reject) => {
            client.service('api/articles').create({
                title,
                content,
                status
            }).then((response) => {
                resolve(response)
                dispatch('FETCH_ARTICLES', {page: 1})
            }).catch((err) => {
                reject(err)
            })
          })
    },

    FETCH_ARTICLE: ({ commit, state }, { id }) => {
        return new Promise((resolve, reject) => {
            client.service('api/articles').get(id).then(response => {
                commit('SET_ARTICLE', { response })
                resolve()
            }).catch((err) => {
                reject(err.message)
            })
        })
    },

    UPDATE_ARTICLE: ({ commit, dispatch, state }, { id, title, content, status }) => {
        return new Promise((resolve, reject) => {
            client.service('api/articles').patch(id, {
                title,
                content,
                status
            }).then((response) => {
                commit('SET_ARTICLE', { response })
                resolve('Post was updated successfully!')
            }).catch((err) => {
                reject(err)
            })
        })
    },

    REMOVE_ARTICLE: ({ commit, dispatch, state }, { id }) => {
        return new Promise((resolve, reject) => {
            client.service('api/articles').remove(id).then((response) => {
                resolve('Post was removed successfully!')
            }).catch((err) => {
                reject(err)
            })
        })
    }
  },

  mutations: {
    SET_ARTICLES: (state, { response }) => {
        state.total = response.total
        state.limit = response.limit
        state.skip = response.skip
        state.data = response.data
    },

    SET_CURRENT_PAGE: (state, currentPage) => {
        state.currentPage = currentPage
    },

    SET_ARTICLE: (state, { response }) => {
        state.single = response
    }
  },

  getters: {

  }
}