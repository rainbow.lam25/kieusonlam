import client from '../feathers';

export default {
  state: {
    accessToken: null,
    authenticated: false,
    isAuthenticating: false,
    profile: null
  },
  actions: {
    nuxtServerInit ({ dispatch }, { req }) {
        console.log('nuxtServerInit');
    },
    CHECK_LOGIN: ({ commit, state }) => {
        return new Promise((resolve, reject) => {
            client.authenticate()
                .then(response => {
                    commit('SET_PROFILE', response)
                    commit('LOGIN_SUCCESS', response)
                    resolve()
                })
                .catch(error => reject(error))
        })
    },

    SIGNUP: ({ commit }, { email, password }) => {
        return new Promise((resolve, reject) => {
            client.service('api/users')
                .create({ email, password })
                .then(response => resolve(response))
                .catch(error => reject(error))
        })
    },

    LOGIN: ({ commit, state }, { email, password }) => {
        return new Promise((resolve, reject) => {
            commit('LOGIN_REQUEST')
            client.authenticate({
                strategy: 'local',
                email,
                password
            })
            .then(response => {
                console.log('Authenticated!', response);
                commit('LOGIN_SUCCESS', response)
                resolve()
                return client.passport.verifyJWT(response.accessToken);
            })
            .then(payload => {
                console.log('JWT Payload', payload);
                return client.service('api/users').get(payload.userId);
            })
            .then(user => {
                client.set('user', user);
                console.log('User', client.get('user'));
                commit('SET_PROFILE', client.get('user'))
            })
            .catch(function(error){
                console.error('Error authenticating!', error);
                commit('LOGIN_FAILURE');
                reject(error);
            });
        })
    },

    LOGOUT: ({ commit }) => {

    }
  },

  mutations: {
    LOGIN_REQUEST: (state) => {
        state.isAuthenticating = true
    },

    LOGIN_FAILURE: (state) => {
        state.isAuthenticating = false
    },

    LOGIN_SUCCESS: (state, response) => {
        state.accessToken = response.accessToken
        state.isAuthenticating = false
        state.authenticated = true
    },

    LOGOUT: (state) => {

    },

    SET_PROFILE: (state, response) => {
        state.profile = { email: response.email }
    }
  },

  getters: {

  }
}